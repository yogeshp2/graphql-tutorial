import graphene
from graphene_django import DjangoObjectType
from graphene_django.fields import DjangoListField

from .models import Quizzes, Question, Answer
from .models import Catagory


class CategoryType(DjangoObjectType):
    class Meta:
        model = Catagory
        fields = ('id', 'name')


class QuizzesType(DjangoObjectType):
    class Meta:
        model = Quizzes
        fields = ("id", "title", "category", "quiz")


class QuestionType(DjangoObjectType):
    class Meta:
        model = Question
        fields = ("title", "quiz")


class AnswerType(DjangoObjectType):
    class Meta:
        model = Answer
        fields = ("question", "answer_text")


class Query(graphene.ObjectType):
    quiz = graphene.String()  # simple
    all_quiz_1 = DjangoListField(QuizzesType)  # directly data retried .

    all_answers = graphene.List(AnswerType,
                                id=graphene.Int())  # fetching data from id custom , more then 1 Ans --> thts y we r using List
    all_questions = graphene.Field(QuestionType, id=graphene.Int())  # fetching data from id custom

    all_quiz_list = graphene.List(QuizzesType)

    def resolve_quiz(root, info):  # root and info min' u have to give in any resolver
        return f'This is 1st question '

    def resolve_all_questions(root, info, id):
        return Question.objects.get(
            pk=id)  # note: get will select only one item so if u want to fectch more thn 1 data then use filter

    def resolve_all_answers(root, info, id):
        return Answer.objects.filter(question=id)

    # def resolve_all_quiz_1(self, info):

    def resolve_all_quiz_list(root, info):
        return Quizzes.objects.all()


class CategoryMutation(graphene.Mutation):  # 1
    class Arguments:
        name = graphene.String(required=True)

    category = graphene.Field(CategoryType)

    @classmethod
    def mutate(cls, root, info, name):  # cls add for performing action like here update
        category = Catagory(name=name)
        category.save()
        return CategoryMutation(category=category)


class CategoryUpdateMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        name = graphene.String(required=True)

    category = graphene.Field(CategoryType)

    @classmethod
    def mutate(cls, root, info, name, id):
        category = Catagory.objects.get(id=id)
        category.name = name
        category.save()
        return CategoryUpdateMutation(category=category)
class CategoryDeleteMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()

    category = graphene.Field(CategoryType)

    @classmethod
    def mutate(cls, root, info, id):
        category = Catagory.objects.get(id=id)
        category.delete()
        return CategoryDeleteMutation(category=category)


class Mutation(graphene.ObjectType):
    add_in_category = CategoryMutation.Field()  # addng into db     #1
    # i/p
    # mutation firstmutation{
    #   updateCategory(name: "newcategory"){
    #      category{
    #       name
    #     }
    #   }
    # }
    # o/p
    # {
    #   "data": {
    #     "updateCategory": {
    #       "category": {
    #         "name": "newcategory"
    #       }
    #     }
    #   }
    # }
    update_category = CategoryUpdateMutation.Field()  # updating db #2
    #i/p
    # mutation
    # {
    #     updateCategory(id: 5 name: "updated")
    # {
    #     category
    # {
    #     id
    #     name
    # }
    # }
    # }
    # #o/p
    # {
    #     "data": {
    #         "updateCategory": {
    #             "category": {
    #                 "id": "5",
    #                 "name": "updated"
    #             }
    #         }
    #     }
    # }
    delete_category = CategoryDeleteMutation.Field()



schema = graphene.Schema(query=Query, mutation=Mutation)
