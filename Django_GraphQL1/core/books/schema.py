import graphene
from graphene_django import DjangoObjectType
from .models import Books


class BooksType(DjangoObjectType):
    class Meta:
        model = Books
        fields = ('id', 'title', 'excerpt')


class Query(graphene.ObjectType):
    all_books = graphene.List(BooksType)
    filted_books = graphene.List(BooksType)

    def resolve_all_books(root, info):
        # return Books.objects.all()        # retried all data
        # return Books.objects.filter(title='fastapi')  # retried which title has fastapi name
        return Books.objects.all()

    def resolve_filted_books(root, info):
        # return Books.objects.all()        # retried all data
        return Books.objects.filter(title='fastapi')  # retried which title has fastapi name


schema = graphene.Schema(query=Query)
